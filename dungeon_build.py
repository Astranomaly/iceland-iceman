
class Dungeon:
    BOARD_X = 18
    BOARD_Y = 13
    CELLS = []

    def __init__( self ):
        print( ">> DUNGEON INIT <<" )
        self.build_cells()

    def __str__( self ):
        return '{}'.format(self.CELLS)

    # generate cells
    def build_cells( self ):
        print( ">> BUILDING CELLS <<" )
        x = 0
        while x < self.BOARD_X:
            y = 0
            while y < self.BOARD_Y:
                cell = ( x, y )
                self.CELLS.append( cell )
                y +=1
            x +=1
    # create walls
    # create tiles
    # place player
    # place monster


class ClassicBoard:
    CELLS = ['##################',
             '#w           #####',
             '#             ####',
             '#  o     ++    ###',
             '#        ++  o  ##',
             '#       o        #',
             '#        o    o  #',
             '#      o         #',
             '##   o       o   #',
             '###  ++++      o #',
             '####  o  o       #',
             '#####           x#',
             '##################']

    def __init__( self ):
        for i in range(len(self.CELLS)):
            print( self.CELLS[i] )
